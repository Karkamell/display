/*
 * main.c
 *
 *  Created on: 2010-03-31
 *       Autor: Miros�aw Karda�
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include "lcd.h"

// poni�sz� linijk� czasami trzeba wpisa� w eclipse przed definicjami
// zmiennych w pami�ci EEPROM, �eby nie podkre�la� sk�adni jako b��dnej
//#define EEMEM __attribute__((section(".eeprom")))

#define LED_PIN (1<<PB0)
#define LED_ON PORTB &= ~LED_PIN
#define LED_OFF PORTB |= LED_PIN


char tab1[] = {"test"};

int main(void)
{
	DDRB |= LED_PIN;

	lcd_init();

	lcd_str("test");				// napis z pami�ci FLASH
	//lcd_locate(0,10);
	//lcd_locate(1,0);
	//lcd_str_E(tab2);				// napis z pami�ci EEPROM
	//lcd_locate(1,10);
	//lcd_str("Linia2");				// napis z pami�ci RAM

	while(1)
	{
		LED_ON;
		_delay_ms(1000);
		LED_OFF;
		_delay_ms(1000);
	}
}

